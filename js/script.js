"use strict";

let firstChild = document.querySelector(".images-wrapper").firstElementChild;
const startBtn = document.querySelector(".wrap-btn-start");
const stopBtn = document.querySelector(".wrap-btn-stop");
let interval = setInterval(changeImg, 3000);


function changeImg() {
  let nextElement = firstChild.nextElementSibling;
  if (nextElement === null) {
    nextElement = document.querySelector(".images-wrapper").firstElementChild;
  }

  function nextElem(element) {
    if (!element.classList.contains("hide-image")) {
      let previusElement = nextElement.previousElementSibling;
      nextElement.classList.remove("hide-image");
      if (previusElement) {
        previusElement.classList.add("hide-image");
      } else {
        previusElement = element;
        previusElement.classList.add("hide-image");
      }
    }
  }

  nextElem(firstChild);
  firstChild = nextElement;
};

startBtn.addEventListener("click", () => {
    interval = setInterval(changeImg, 3000);


})

stopBtn.addEventListener('click', () => {
    clearInterval(interval);
})